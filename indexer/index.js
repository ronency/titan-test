const request = require('request');
const cheerio = require('cheerio')
var DB = require('../DB');

function indexPage(url, body){
	const $ = cheerio.load(body)
	let title = $('title').text();
	let entry = {url, title};
	DB.addPage(entry);
}

module.exports = {
	indexPage,
};
