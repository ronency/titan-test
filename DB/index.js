var _DB = {
	pages: [], 
	searchIndex: {}
};
var searchIndex = {};

function addPage(entry) {
	_DB.pages.push(entry);
	addToIndex(entry);
}

function addToIndex(entry){
	/* in this searchIndex, the keys are the search queries, to optimize the runtime performance */
	entry.title.toLowerCase().split(' ').forEach(function(word){
		if(!!_DB.searchIndex[word]) {// word exist in the index
			if(_DB.searchIndex[word].indexOf(entry.url) == -1){ // url is "new" for this word
				_DB.searchIndex[word].push(entry.url);
			}
		}
		else {// word is a "new" query, create it for the first time
			_DB.searchIndex[word] = [entry.url];
		}

	});
}

function print(){
	console.log('DB.pages: ', _DB.pages);
	console.log('DB.searchIndex: ', _DB.searchIndex);
	let q = 'for';
	console.log(`results For ${q}: `, search(q));
}

/* returns array of URLs */
function search(q) {
	if (!!_DB.searchIndex[q])
		return _DB.searchIndex[q];
	else
		return [];
}

module.exports = {
	addPage,
	search,
	print
};
