var express = require('express');  
var app = express();  
var server = require('http').createServer(app);  
var io = require('socket.io')(server);
var request = require("request");
var crawler = require('./crawler');
var indexer = require('./indexer'); 
var DB = require('./DB');
var baseURL = 'https://bitcoin.org';

app.use(express.static(__dirname + '/node_modules'));
app.use(express.static(__dirname + '/static'));
app.get('/', function(req, res, next) {  
  let q = req.query.q;
  if(!!q) {
    let results = DB.search(q);
    if(results.length == 0){
      res.send(`No results found for [${q}]`);
    }else {
      let html = [];
      results.forEach(function(url){
        html.push(`<div>${url}</div>`)
      })
      res.send(html.join(''));
    }
  }else {
    res.send('Add "?q=" to the url for search');
  }
});

crawler.scanUrl(baseURL);

server.listen(process.env.PORT || 1234);
