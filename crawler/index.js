const request = require('request');
const cheerio = require('cheerio')
var indexer = require('../indexer'); 
var DB = require('../DB'); 
var limit = 20, 
	scannedPages = 0;

function extractLinks(baseUrl, body) {
	const $ = cheerio.load(body)
  	let links = [];
  	$('a').each(function(i, element){
  		let url = element.attribs['href'];
  		if(!!url) {
  			if (url.indexOf('http') == -1 && url.indexOf('bitcoin:') == -1) {
  				url = baseUrl + url;
  			}
  			if(scannedPages++ <= limit)
  				scanUrl(url);
  		}
  	})

}


function scanUrl(baseUrl){
	console.log('fetching: ', baseUrl);
    request({
      url: baseUrl,
      json: true
    }, function (error, response, body) {
      if (!error && response.statusCode === 200) {
		indexer.indexPage(baseUrl, body);
		if(scannedPages++ <= limit)
			extractLinks(baseUrl, body);
		else
			DB.print();
      }
    });
}

module.exports = {
	scanUrl: scanUrl
};

